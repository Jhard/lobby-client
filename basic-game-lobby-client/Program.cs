﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;

namespace basic_game_lobby_client
{
    class Program
    {
        private const int listenPort = 9000;
        private const string HostName = "192.168.0.14";
        static void Main(string[] args)
        {
            try
            {
                IPAddress address = IPAddress.Parse(HostName);
                IPEndPoint enpoint = new IPEndPoint(address, listenPort);

                var client = new TcpClient();
                client.Connect(HostName, listenPort);

                StreamWriter stream = new StreamWriter(client.GetStream());
                stream.AutoFlush = true;
                stream.WriteLine("Jhard");

                StreamReader reader = new StreamReader(client.GetStream());

                int pingCount = 0;
                while (pingCount < 10)
                {
                    string message = reader.ReadLine();
                    if (message == "ping")
                    {
                        stream.WriteLine("ping");
                        Console.WriteLine("Recieved ping!");
                        pingCount++;
                    }
                }


                stream.Close();
                client.Close();

            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }
    }
}
